package com.curso.transport;

public class Airplane implements ITransport {

	private String constructor;
	private String model;

	public Airplane() {
		super();
	}

	public Airplane(String constructor, String model) {
		super();
		this.constructor = constructor;
		this.model = model;
	}

	@Override
	public void fly(String origin, String destination, int kilometers) {
		System.out.println("Vuelo en curso: " + this.toString() + ": " + origin + ">" + destination + ">" + kilometers);

	}

	public void horizontalTakeoff() {
		System.out.println("El Avion " + this.toString() + " esta despegando");
	}

	public String getConstructor() {
		return constructor;
	}

	public void setConstructor(String constructor) {
		this.constructor = constructor;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Airplane [constructor=" + constructor + ", model=" + model + "]";
	}

}
