package com.curso.transport;

public interface ITransport {

	public void fly(String origin, String destination, int kilometers);
	
}
