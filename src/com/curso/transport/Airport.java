package com.curso.transport;

import java.util.ArrayList;
import java.util.List;

public class Airport {

	private String nombre;
	private String ciudad;
	private List<ITransport> itransportList = new ArrayList<>();

	public Airport() {
		super();
	}

	public Airport(String nombre, String ciudad, List<ITransport> itransportList) {
		super();
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.itransportList = itransportList;
	}

	public void acceptTransport(ITransport transport) {
		// Tb podemos hacerlo this.itransportList.add(transport);
		this.getItransportList().add(transport);
	}

	public void removeTransport(ITransport transport) {
		// Tb podemos hacerlo this.itransportList.remove(transport);
		this.getItransportList().remove(transport);
	}

	public void showTransportList() {
		for (int i = 0; i < this.getItransportList().size(); i++) {
			System.out.println("\t " + this.getItransportList().get(i));
		}
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public List<ITransport> getItransportList() {
		return itransportList;
	}

	public void setItransportList(List<ITransport> itransportList) {
		this.itransportList = itransportList;
	}

	@Override
	public String toString() {
		return "Airport [nombre=" + nombre + ", ciudad=" + ciudad + ", itransportList=" + itransportList + "]";
	}

}
