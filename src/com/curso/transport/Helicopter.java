package com.curso.transport;

public class Helicopter implements ITransport {

	private String constructor;
	private String model;

	public Helicopter() {
		super();
	}

	public Helicopter(String constructor, String model) {
		super();
		this.constructor = constructor;
		this.model = model;
	}

	@Override
	public void fly(String origin, String destination, int kilometers) {
		System.out.println("Vuelo en curso: " + this.toString() + ": " + origin + ">" + destination + ">" + kilometers);

	}

	public void verticalTakeoff() {
		System.out.println("El helicoptero " + this.toString() + " esta despegando");
	}

	public String getConstructor() {
		return constructor;
	}

	public void setConstructor(String constructor) {
		this.constructor = constructor;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Helicopter [constructor=" + constructor + ", model=" + model + "]";
	}

}
