package com.curso.transport;

public class Unicord implements ITransport {

	private int numberOfLegs;
	private String color;

	public Unicord() {
		super();
	}

	public Unicord( String color, int numberOfLegs) {
		super();
		this.numberOfLegs = numberOfLegs;
		this.color = color;
	}

	@Override
	public void fly(String origin, String destination, int kilometers) {
		System.out.println("Vuelo en curso: " + this.toString() + ": " + origin + ">" + destination + ">" + kilometers);

	}

	public void eat() {
		System.out.println("El Unicornio " + this.toString() + " esta comiendo");
	}

	public int getNumberOfLegs() {
		return numberOfLegs;
	}

	public void setNumberOfLegs(int numberOfLegs) {
		this.numberOfLegs = numberOfLegs;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Unicord [numberOfLegs=" + numberOfLegs + ", color=" + color + "]";
	}

}
