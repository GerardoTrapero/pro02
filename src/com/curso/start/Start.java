package com.curso.start;

import java.util.ArrayList;

import com.curso.transport.Airplane;
import com.curso.transport.Airport;
import com.curso.transport.Helicopter;
import com.curso.transport.ITransport;
import com.curso.transport.Unicord;

public class Start {

	public static void main(String[] args) {
		System.out.println("Inicio del Programa de Aviones y Aeropuertos");
		testTransport();
		System.out.println("Fin del Programa de Aviones y Aeropuertos");
	}

	public static void testTransport() {
		// List<ITransport> itransporList = new ArrayList<>();
		// Airport airport1 = new Airport("Adolfo Suarez", "Madrid", itransporList);
		
		Airport airport1 = new Airport("Adolfo Suarez",  "Madrid", new ArrayList<ITransport>());
		
		//ITransport transport1 = new Airplane("Boeing", "747");
		
		// Creación de objetos anónimos
		airport1.acceptTransport(new Airplane("Boeing", "747"));
		
		
		//airport1.acceptTransport(transport2);
		//
		airport1.acceptTransport(new Helicopter ("Douglas", "Apache"));
		
		airport1.acceptTransport(new Unicord ("Azul", 5));
		
		airport1.acceptTransport(new Unicord ("Rosa", 7));	
		
		airport1.showTransportList();
	
	
	}

}
